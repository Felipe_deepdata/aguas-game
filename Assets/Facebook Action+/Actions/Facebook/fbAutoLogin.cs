﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;

//[InitializeOnLoad]
public class fbAutoLogin : MonoBehaviour
{
    void Awake()
    {
        StartCoroutine(log());
    }

    IEnumerator log()
    {
        bool fbLog = true;
        while (!FB.IsLoggedIn)
        {
            bool fbInit = true;
            while (!FB.IsInitialized)
            {
                if (fbInit)
                {
                    init();
                    fbInit = false;
                    //Debug.Log("Start Init");
                }
                yield return new WaitForSeconds(1.0f);
            }

            if (fbLog && FB.IsInitialized)
            {
                //Debug.Log("Init OK");
                FB.LogInWithReadPermissions(null, AuthCallback);
                fbLog = false;
                //Debug.Log("Start Log");
            }
            yield return new WaitForSeconds(1.0f);
        }

        //if (FB.IsLoggedIn)
        //{
        //Debug.Log("Log OK");
        //}
    }

    void logInWithReadPermissions()
    {
        if (!FB.IsInitialized)
        {
            init();
        }

        //var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(null, AuthCallback);
    }

    void init()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
    }

    void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
            Debug.Log("Init Facebook OK");
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            var aToken = AccessToken.CurrentAccessToken;
            Debug.Log(aToken.UserId);

            //foreach (string perm in aToken.Permissions)
            //{
            //Debug.Log(perm);
            //}
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

}