﻿using Facebook.Unity;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Facebook")]
    [Tooltip("Facebook Graph Request Retrieve Profile Photo")]
    public class fbProfilePhoto : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string profilePhotoDes = "Facebook Graph Request Retrieve Profile Photo.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Results")]
		public FsmTexture fbResultProfilePhoto;

		[UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
		{
            fbResultProfilePhoto = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

		public override void OnGUI()
		{
			if (fbResultProfilePhoto.Value == null)
			{
				return;
			}

			Rect rect = new Rect (new Vector2 (Screen.width, Screen.height), new Vector2 (Screen.width, Screen.height));
			ScaleMode scaleMode = ScaleMode.StretchToFill;

			GUI.DrawTexture (rect, fbResultProfilePhoto.Value, scaleMode, true, 0);

        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            FB.API("/me/picture", HttpMethod.GET, callback: ProfilePhotoCallback);
        }

        void ProfilePhotoCallback(IGraphResult result)
        {
            if (string.IsNullOrEmpty(result.Error) && result.Texture != null)
            {
                fbResultProfilePhoto.Value = result.Texture;

            }

            handleResult(result);
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }

    [ActionCategory("Facebook")]
    [Tooltip("Facebook Graph Request Take And Upload Screenshot")]
    public class fbGraphTakeUpload : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string profilePhotoDes = "Facebook Graph Request Take And Upload Screenshot.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        [RequiredField]
        public FsmString fbGraphTakeUploadMessage;

        [ActionSection("Results")]
		public FsmTexture fbResultTakePhoto;

		[UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
		{
            fbGraphTakeUploadMessage = null;
            fbResultTakePhoto = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

		public override void OnGUI()
		{
			if (fbResultTakePhoto.Value == null)
			{
				return;
			}

			Rect rect = new Rect (new Vector2 (Screen.width, Screen.height), new Vector2 (Screen.width, Screen.height));
			ScaleMode scaleMode = ScaleMode.StretchToFill;

			GUI.DrawTexture (rect, fbResultTakePhoto.Value, scaleMode, true, 0);
		}

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            var width = Screen.width;
            var height = Screen.height;
            var tex = new Texture2D(width, height, TextureFormat.RGB24, false);

            // Read screen contents into the texture
            tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
            tex.Apply();
            byte[] screenshot = tex.EncodeToPNG();

            var wwwForm = new WWWForm();
            wwwForm.AddBinaryData("image", screenshot, "InteractiveConsole.png");
            fbResultTakePhoto.Value = tex;
            wwwForm.AddField("message", fbGraphTakeUploadMessage.Value);
            FB.API("me/photos", HttpMethod.POST, handleResult, wwwForm);
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }

//    [ActionCategory("Facebook")]
//    [Tooltip("Facebook Graph Basic Request")]
//    public class fbGraphBasicRequest : FsmStateAction
//    {
//        [ActionSection("Results")]
//        public FsmString fbResultGraphBasicRequest;
//
//        [ActionSection("Events")]
//        public FsmEvent successEvent;
//        public FsmEvent errorEvent;
//        public FsmEvent cancelEvent;
//
//        public override void OnEnter()
//        {
//            if (!FB.IsInitialized || !FB.IsLoggedIn)
//            {
//                if (FsmEvent.IsNullOrEmpty(errorEvent))
//                    Fsm.Event(errorEvent);
//            }
//            else
//            {
//                FB.API("/me", HttpMethod.GET, handleResult);
//            }
//
//            Finish();
//        }
//
//        void handleResult(IResult result)
//        {
//            if (!string.IsNullOrEmpty(result.Error))
//            {
//                fbResultGraphBasicRequest.Value = result.Error;
//                if (FsmEvent.IsNullOrEmpty(errorEvent))
//                    Fsm.Event(errorEvent);
//            }
//            else if (result.Cancelled)
//            {
//                fbResultGraphBasicRequest.Value = result.RawResult;
//                if (FsmEvent.IsNullOrEmpty(cancelEvent))
//                    Fsm.Event(cancelEvent);
//            }
//            else if (!string.IsNullOrEmpty(result.RawResult))
//            {
//                fbResultGraphBasicRequest.Value = result.RawResult;
//                if (FsmEvent.IsNullOrEmpty(successEvent))
//                    Fsm.Event(successEvent);
//            }
//            else
//            {
//                fbResultGraphBasicRequest.Value = "Empty Response";
//            }
//        }
//    }
//
//    [ActionCategory("Facebook")]
//    [Tooltip("Facebook Graph Custom Request")]
//    public class fbGraphCustomRequest : FsmStateAction
//    {
//        [RequiredField]
//        public FsmString fbGraphCustomRequestString;
//
//        [ActionSection("Results")]
//        public FsmString fbResultGraphCustomRequest;
//
//        [ActionSection("Events")]
//        public FsmEvent successEvent;
//        public FsmEvent errorEvent;
//        public FsmEvent cancelEvent;
//
//        public override void OnEnter()
//        {
//            if (!FB.IsInitialized || !FB.IsLoggedIn)
//            {
//                if (FsmEvent.IsNullOrEmpty(errorEvent))
//                    Fsm.Event(errorEvent);
//            }
//            else
//            {
//                FB.API(fbGraphCustomRequestString.Value, HttpMethod.GET, handleResult);
//            }
//
//            Finish();
//        }
//
//        void handleResult(IResult result)
//        {
//            if (!string.IsNullOrEmpty(result.Error))
//            {
//                fbResultGraphCustomRequest.Value = result.Error;
//                if (FsmEvent.IsNullOrEmpty(errorEvent))
//                    Fsm.Event(errorEvent);
//            }
//            else if (result.Cancelled)
//            {
//                fbResultGraphCustomRequest.Value = result.RawResult;
//                if (FsmEvent.IsNullOrEmpty(cancelEvent))
//                    Fsm.Event(cancelEvent);
//            }
//            else if (!string.IsNullOrEmpty(result.RawResult))
//            {
//                fbResultGraphCustomRequest.Value = result.RawResult;
//                if (FsmEvent.IsNullOrEmpty(successEvent))
//                    Fsm.Event(successEvent);
//            }
//            else
//            {
//                fbResultGraphCustomRequest.Value = "Empty Response";
//            }
//        }
//    }
}