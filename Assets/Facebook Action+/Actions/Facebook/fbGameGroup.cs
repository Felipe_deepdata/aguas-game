﻿using UnityEngine;
using Facebook.Unity;
using System.Collections.Generic;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Facebook")]
    [Tooltip("Facebook Create Group Of Game")]
    public class fbGroupCreate : FsmStateAction
    {
        public class typePrivacy
        {
            public enum Privacy { OPEN, CLOSED }
        }

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        public typePrivacy.Privacy privacy = typePrivacy.Privacy.CLOSED;

        [UIHint(UIHint.Description)]
        public string createGroupDes = "The first step in integrating groups into your game is giving players the ability to create their own group. With this feature you can set parameters like name, description and privacy.";

        [RequiredField]
        public FsmString fbGroupCreateName;
        public FsmString fbGroupCreateDescription;

        [ActionSection("Results")]
        [UIHint(UIHint.Variable)]
        public FsmString fbResultGroupID;

        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbGroupCreateName = null;
            fbGroupCreateDescription = null;
            fbResultGroupID = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            FB.GameGroupCreate(fbGroupCreateName.Value, fbGroupCreateDescription.Value, privacy.ToString(), handleResult);
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                var jsonJesult = Facebook.MiniJSON.Json.Deserialize(result.RawResult) as Dictionary<string, object>;

                if (jsonJesult.ContainsKey("id"))
                    fbResultGroupID.Value = (jsonJesult["id"] as object).ToString();

                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }

    [ActionCategory("Facebook")]
    [Tooltip("Facebook Join To Game Group")]
    public class fbJoinGroup : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string joinGroupDes = "After viewing a group, a player may want to join. Games should provide browse or search functionality to help players find groups. Games could also suggest groups for the player to join in order to optimize the game experience (for example, based on geographical location or skill level).";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        [RequiredField]
        public FsmString fbJoinGroupID;

        [ActionSection("Results")]
        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbJoinGroupID = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            FB.GameGroupJoin(fbJoinGroupID.Value, handleResult);
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }

    [ActionCategory("Facebook")]
    [Tooltip("Facebook Get All App Managed Groups")]
    public class fbAdministeringGroups : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string adminGroupDes = "Get list of game groups that the user is management.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Results")]
        [UIHint(UIHint.Variable)]
        public FsmArray fbResultAdminGroupID;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbResultAdminGroupID = null;
            successEvent = null;
            errorEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            FB.API(FB.AppId + "/groups", HttpMethod.GET, GetAllGroupsCB);
        }
        
        void GetAllGroupsCB(IGraphResult result)
        {
            if (!string.IsNullOrEmpty(result.RawResult))
            {
                //result.RawResult;
                var resultDictionary = result.ResultDictionary;
                if (resultDictionary.ContainsKey("data"))
                {
                    var dataArray = (List<object>)resultDictionary["data"];
                    string stringToSplit = "";
                    if (dataArray.Count > 0)
                    {
                        for (int i = 0; i < dataArray.Count; i++)
                        {
                            var firstGroup = (Dictionary<string, object>)dataArray[i];
                            stringToSplit += "," + (string)firstGroup["id"];
                        }
                    }

                    if(stringToSplit.Length > 0)
                    {
                        strSplit(stringToSplit.Substring(1));
                    }
                }

                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }

            if (!string.IsNullOrEmpty(result.Error))
            {
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }

            Finish();
        }

        void strSplit(string stringToSplit)
        {
            var trimCharsArray = ",".ToCharArray();
            fbResultAdminGroupID.stringValues = stringToSplit.Split(",".ToCharArray());
            for (var i = 0; i < fbResultAdminGroupID.stringValues.Length; i++)
            {
                var s = fbResultAdminGroupID.stringValues[i];
                fbResultAdminGroupID.Set(i, s.Trim(trimCharsArray));
            }
            fbResultAdminGroupID.SaveChanges();
        }
    }

    //[ActionCategory("Facebook")]
    //[Tooltip("Facebook Get Gamer Groups Logged In User Belongs To")]
    //public class fbUseInGroup : FsmStateAction
    //{
    //    [UIHint(UIHint.Description)]
    //    public string UseInGroupDes = "";

    //    [ActionSection("Results")]
    //    [UIHint(UIHint.Description)]
    //    public string fbRawResult;

    //    [ActionSection("Events")]
    //    public FsmEvent successEvent;
    //    public FsmEvent errorEvent;
    //    public FsmEvent cancelEvent;

    //    public override void Reset()
    //    {
    //        fbRawResult = null;
    //        successEvent = null;
    //        errorEvent = null;
    //        cancelEvent = null;
    //    }

    //    public override void OnEnter()
    //    {
    //        if (!FB.IsInitialized || !FB.IsLoggedIn)
    //        {
    //            if (FsmEvent.IsNullOrEmpty(errorEvent))
    //                Fsm.Event(errorEvent);
    //        }
    //        else
    //        {
    //            FB.API("/me/groups?parent=" + FB.AppId, HttpMethod.GET, handleResult);
    //        }

    //        Finish();
    //    }

    //    void handleResult(IResult result)
    //    {
    //        if (!string.IsNullOrEmpty(result.Error))
    //        {
    //            fbRawResult = result.Error;
    //            if (FsmEvent.IsNullOrEmpty(errorEvent))
    //                Fsm.Event(errorEvent);
    //        }
    //        else if (result.Cancelled)
    //        {
    //            fbRawResult = result.RawResult;
    //            if (FsmEvent.IsNullOrEmpty(cancelEvent))
    //                Fsm.Event(cancelEvent);
    //        }
    //        else if (!string.IsNullOrEmpty(result.RawResult))
    //        {
    //            fbRawResult = result.RawResult;
    //            if (FsmEvent.IsNullOrEmpty(successEvent))
    //                Fsm.Event(successEvent);
    //        }
    //        else
    //        {
    //            fbRawResult = "Empty Response";
    //        }
    //    }
    //}

    [ActionCategory("Facebook")]
    [Tooltip("Facebook Make Group Post As User")]
    public class fbMakeGroup : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string postGroupDes = "Game Groups provide a space where players can interact with each other or content posted from the game, keeping them engaged. Games can post stories to groups as the game or on behalf of the user. You should use the type of sharing that’s appropriate to the experience.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        [RequiredField]
        public FsmString fbMakeGroupID;
        [RequiredField]
        public FsmString fbMakeGroupMessage;

        [ActionSection("Results")]
        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbMakeGroupID = null;
            fbMakeGroupMessage = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict["message"] = fbMakeGroupMessage.Value;

            FB.API(
                fbMakeGroupID.Value + "/feed",
                HttpMethod.POST,
                handleResult,
                dict);
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }
}
