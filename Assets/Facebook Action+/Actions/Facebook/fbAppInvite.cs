﻿using UnityEngine;
using Facebook.Unity;
using System;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory("Facebook")]
    [Tooltip("Facebook AppInvite")]

    public class fbAppInvite : FsmStateAction
    {
        [UIHint(UIHint.Description)]
        public string InviteDes = "Invitation installed application is a personal approach, and rich content to people invite their friends on Facebook using a mobile app.";

        [ActionSection("Auto Login")]
        public FsmBool fbAutoLogin;

        [ActionSection("Inputs")]
        public FsmString fbAppInviteLinkURL;
        public FsmString fbAppInviteImageURL;

        [ActionSection("Results")]
        [UIHint(UIHint.Description)]
        public string fbRawResult;

        [ActionSection("Events")]
        public FsmEvent successEvent;
        public FsmEvent errorEvent;
        public FsmEvent cancelEvent;

        float timeRate = 0.2f, nextTime = 0.0f;
        bool autoLogFace = true, run_func = true, isLogin = true;

        public override void Reset()
        {
            fbAppInviteLinkURL = null;
            fbAppInviteImageURL = null;
            fbRawResult = null;
            successEvent = null;
            errorEvent = null;
            cancelEvent = null;
            fbAutoLogin = null;
            timeRate = 0.2f; nextTime = 0.0f;
            autoLogFace = true; run_func = true;
        }

        public override void OnUpdate()
        {
            if (FB.IsLoggedIn && isLogin)
            {
                if (run_func)
                {
                    _function();
                    run_func = false;
                    isLogin = false;
                }
            }

            if (run_func)
            {
                if (fbAutoLogin.Value)
                {
                    if (autoLogFace)
                    {
                        Fsm.GameObject.AddComponent<fbAutoLogin>();
                        autoLogFace = false;
                    }

                    if (!FB.IsLoggedIn)
                    {
                        if (Time.time > nextTime)
                        {
                            nextTime = Time.time + timeRate;
                        }
                    }

                    if (FB.IsLoggedIn)
                    {
                        _function();
                        run_func = false;
                    }
                }
            }
        }

        public override void OnEnter()
        {
            if (!fbAutoLogin.Value)
            {
                _function();
            }
        }

        private void _function()
        {
            string link;
            if (string.IsNullOrEmpty(fbAppInviteLinkURL.Value))
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    link = "https://fb.me/892708710750483";
                }
                else
                {
                    link = "https://fb.me/810530068992919";
                }
            }
            else
            {
                link = fbAppInviteLinkURL.Value;
            }
            if (string.IsNullOrEmpty(fbAppInviteImageURL.Value))
            {
                FB.Mobile.AppInvite(new Uri(link), callback: handleResult);
            }
            else
            {
                FB.Mobile.AppInvite(new Uri(link), new Uri(fbAppInviteImageURL.Value), handleResult);
            }
        }

        void handleResult(IResult result)
        {
            if (!string.IsNullOrEmpty(result.Error))
            {
                fbRawResult = result.Error;
                if (FsmEvent.IsNullOrEmpty(errorEvent))
                    Fsm.Event(errorEvent);
            }
            else if (result.Cancelled)
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(cancelEvent))
                    Fsm.Event(cancelEvent);
            }
            else if (!string.IsNullOrEmpty(result.RawResult))
            {
                fbRawResult = result.RawResult;
                if (FsmEvent.IsNullOrEmpty(successEvent))
                    Fsm.Event(successEvent);
            }
            else
            {
                fbRawResult = "Empty Response";
            }

            Finish();
        }
    }
}