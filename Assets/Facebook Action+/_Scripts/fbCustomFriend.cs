﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Facebook.Unity;
using System.Collections.Generic;
using Facebook.MiniJSON;
using System;

public class fbCustomFriend : MonoBehaviour {

    // Use this for initialization
    public GameObject objCustomDialog;
    public string apiFriend = "me/invitable_friends";
    private Canvas ListFriends;
    private GameObject ItemFriend;
    private listFriend listFriend;
    private Toggle btnSelectAll;
    public string[] listID;
    private bool isCkAll = true;
    private InputField txt_search;
    private string key_search = "";

    public bool loadFinish = false, selectFinish = false;

    public void start()
    {
        listFriend = new listFriend();
        objCustomDialog = Instantiate(Resources.Load("Canvas_Friend")) as GameObject;
        ScrollRect scrollRect = objCustomDialog.GetComponentInChildren<ScrollRect>();
        ListFriends = scrollRect.GetComponentInChildren<Canvas>();
        ItemFriend = Resources.Load("FriendItem") as GameObject;
        txt_search = objCustomDialog.GetComponentInChildren<InputField>();
        btnSelectAll = objCustomDialog.GetComponentInChildren<Toggle>();
        btnSelectAll.onValueChanged.RemoveAllListeners();
        btnSelectAll.onValueChanged.AddListener((value) =>
        {
            selectAll(value);
        });
        Button btnInvite = objCustomDialog.GetComponentInChildren<Button>();
        btnInvite.onClick.RemoveAllListeners();
        btnInvite.onClick.AddListener(selectFriends);
        if (FB.IsLoggedIn)
        {
            FB.API(apiFriend, HttpMethod.GET, callBackLoadFriend);
        }
    }

    void Update()
    {
        string textSearch = unicode.Remove(txt_search.text.Trim());
        if (txt_search != null && key_search != textSearch)
        {
            key_search = textSearch;
            if (loadFinish)
            {
                foreach (Canvas _item in ListFriends.GetComponentsInChildren<Canvas>())
                {
                    GameObject item = _item.gameObject;
                    if (_item != ListFriends)
                        Destroy(item);
                }

                foreach (_listFriend _friend in listFriend.getAllItem())
                {
                    if (unicode.Remove(_friend.name).IndexOf(key_search) >= 0)
                    {
                        GameObject item = Instantiate(ItemFriend);
                        item.GetComponentInChildren<RawImage>().texture = _friend.avata;
                        item.GetComponentInChildren<Text>().text = _friend.name;
                        Toggle btnSelect = item.GetComponentInChildren<Toggle>();
                        btnSelect.name = _friend.key;
                        btnSelect.onValueChanged.AddListener(delegate { onValuesChange(btnSelect); });
                        item.transform.SetParent(ListFriends.transform);
                    }
                }
            }
        }
    }

    void callBackLoadFriend(IGraphResult result)
    {
        int index = 0;
        Dictionary<string, object> JSON = Json.Deserialize(result.RawResult) as Dictionary<string, object>;
        List<object> data = JSON["data"] as List<object>;

        for (int i = 0; i < data.Count; i++)
        {
            index++;
            string key = "friend" + index;
            Dictionary<string, object> iData = data[i] as Dictionary<string, object>;
            object id = iData["id"] as object;
            object name = iData["name"] as object;

            GameObject item = Instantiate(ItemFriend);
            RawImage img = item.GetComponentInChildren<RawImage>();
            Text txtName = item.GetComponentInChildren<Text>();
            txtName.text = name.ToString();

            Toggle btnSelect = item.GetComponentInChildren<Toggle>();
            btnSelect.name = key;
            btnSelect.onValueChanged.AddListener(delegate { onValuesChange(btnSelect); });

            csLoadImage loadImg = new csLoadImage(listFriend, key, id.ToString(), name.ToString(), img);
            if (apiFriend.Contains("me/friends"))
            {
                FB.API("/" + id.ToString() + "/picture", HttpMethod.GET, loadImg.profilePhoto);
            }
            else
            {
                Dictionary<string, object> picInfo = iData["picture"] as Dictionary<string, object>;
                Dictionary<string, object> picData = picInfo["data"] as Dictionary<string, object>;
                object url = picData["url"] as object;
                StartCoroutine(loadImg.routinePhoto(url.ToString()));
            }

            item.transform.SetParent(ListFriends.transform);
        }
        loadFinish = true;
    }

    private void onValuesChange(Toggle btnSelect)
    {
        listFriend.changeSelect(btnSelect.name, btnSelect.isOn);
        isCkAll = false;
        btnSelectAll.isOn = listFriend.checkAll();
        isCkAll = true;
    }

    private void selectAll(bool ck = false)
    {
        if (isCkAll)
        {
            listFriend.selectAll(btnSelectAll.isOn);
            Canvas[] list = ListFriends.GetComponentsInChildren<Canvas>();
            foreach (Canvas item in list)
            {
                item.GetComponentInChildren<Toggle>().isOn = btnSelectAll.isOn;
            }
        }
    }

    private void selectFriends()
    {
        listID = listFriend.getIdByCheck();
        selectFinish = true;
        objCustomDialog.SetActive(false);
    }
}
