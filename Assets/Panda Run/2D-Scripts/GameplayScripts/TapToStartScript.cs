﻿using UnityEngine;
using System.Collections;
using PandaRun;

public class TapToStartScript : MonoBehaviour {

	public AudioClip ClickSound;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		//on click/touch, disable the Touch To Play Screen
		if((((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))) && GetComponent<Renderer>().enabled == true)
		{
			//GetComponent<Renderer>().enabled = false;
			this.gameObject.SetActive(false);
		}
		//if the character is movine no need to show the Tap To Play screen
		if(PandaRun.CharacterController.canMove == true)
		{
			this.gameObject.SetActive(false);
		}

	}

	public void Activate(){

		GetComponent<AudioSource>().PlayOneShot(ClickSound, 1);
		//Debug.Log ("on mouse down");
		Time.timeScale = 0;
		PandaRun.CharacterController.canMove = true;
		//GetComponent<Renderer>().enabled = false;
		this.gameObject.SetActive(false);
	}

	void OnMouseDown()
	{

		GetComponent<AudioSource>().PlayOneShot(ClickSound, 1);
		//Debug.Log ("on mouse down");
		Time.timeScale = 0;
		PandaRun.CharacterController.canMove = true;
		//GetComponent<Renderer>().enabled = false;
		this.gameObject.SetActive(false);
	}
}
