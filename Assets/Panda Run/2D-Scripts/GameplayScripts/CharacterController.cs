using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using System.Collections.Generic;
using System.Linq;

namespace PandaRun
{

    public class CharacterController : MonoBehaviour
    {

        public float maxSpeed;              //maximum character speed
        Animator anim;                      //we need to access animator to modify speed automaticaly

        public AudioClip pickupCoin;
        public AudioClip jumpSound;
		public AudioClip SplashSound;

        public Transform groundCheck;       //object used for verification of ground
        public float groundRadius;          //how big is our ground sphere
        public float JumpForce;             //the force of our jump on Y axis

        bool DoubleJump = false;
        //bool simpleJump = false;
        public LayerMask whatIsGround;      //what is considered ground ?

        public static bool onGround = false;                //verifies is character is on the ground
        public static bool isStuck = false;
        public static bool isDead = false;
        public static bool closeToFinish = false;

        public static bool canMove = false;            //whether to move or not (also influences the time.timeScale, alternating it between 1 and 0
        public static bool stopCameraMark = false;


        private int lastlevel;

        //private bool canMove = false;
        private float timeBetweenJumps = 0.0f;
        private float curPos = 0.0f;
        private float lastPos = 0.0f;



        private int maxObtainableScore; // maximum amount of score that can be obtained in the current level

        public GameObject connectAPIGO;
        public ConnectAPI connectAPI;

        public GameObject LevelFinishedBackgroundLivePlay, LevelFinishedBackgroundPracticeMode;
		public Text Discount;

		private int Level;

		bool SoundEnabled;


		// The name of the sprite sheet to use
		public string SpriteSheetName;

		// The name of the currently loaded sprite sheet
		private string LoadedSpriteSheetName;

		// The dictionary containing all the sliced up sprites in the sprite sheet
		private Dictionary<string, Sprite> spriteSheet;

		// The Unity sprite renderer so that we don't have to get it multiple times
		private SpriteRenderer spriteRenderer;
		Animator animator;
		Sprite sprite1;
		bool SpriteSet = false;
		float DeathTimer = 1f;

		Rigidbody2D rb;

        void Awake()
        {
            anim = GetComponent<Animator>();
        }

        // Use this for initialization
        void Start()
        {
			
			rb = gameObject.GetComponent<Rigidbody2D>();

			animator = this.GetComponent<Animator>();
			RuntimeAnimatorController newController;

			int Gender = PlayerPrefs.GetInt ("Gender");
			// Debug.Log ("Gender =" + Gender);

			if(Gender == 1)
				newController =  (RuntimeAnimatorController)Resources.Load("MainCharacterAnimator");
			else
				newController =  (RuntimeAnimatorController)Resources.Load("MainCharacter2Animator");
			animator.runtimeAnimatorController = newController;

			
			Level = HelperScript.currentLevelNumber;
			// Debug.Log ("you are in level: "+Level);
            stopCameraMark = false;


            closeToFinish = false;
            isStuck = false;
            isDead = false;
            HelperScript.score = 0;

            HelperScript.levelName = Application.loadedLevelName;

            maxObtainableScore = PlayerPrefs.GetInt(HelperScript.levelName + "_Arguments", 0); //we stored the max obtainable score in player pref for every level
            lastlevel = PlayerPrefs.GetInt ("_Arguments");
			// Debug.Log ("Ultimo nivel completado :V" + HelperScript.currentLevelNumber);
			// Debug.Log ("controller blabla");

            groundRadius = 0.3f;
            canMove = false;
			if (ApplicationModel.LivePlay)
            {
                connectAPIGO = GameObject.Find("ConnectAPI");
                connectAPI = connectAPIGO.GetComponent<ConnectAPI>();



                connectAPI.sumarIntento(() => { Debug.Log("intente una vez"); });
            }


				if (PlayerPrefs.GetInt("SoundEnabled")==0)
					SoundEnabled = true;
				if (PlayerPrefs.GetInt ("SoundEnabled") == 1)
					SoundEnabled = false;
			
        }


		private void SetCharacterSprite(){
			this.spriteRenderer.sprite = sprite1;
			SpriteSet = true;
		}




        //used for animator sync; no need for time.deltatime
        //does not fluctuate; better for physics stuff
        void FixedUpdate()
        {




            //We check every time to see if the character hit what is considered ground or not
            //If true -> character is on permitable ground

            if ((Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround)))
            {
                onGround = true;
            }
            else
            {
                onGround = false;
            }

            //animator needs to know if the character is on the ground
            anim.SetBool("Ground", onGround);

            //how high are we jumping. the speed on Y axis
            anim.SetFloat("VerticalSpeed", GetComponent<Rigidbody2D>().velocity.y);

            if (onGround) { DoubleJump = false; }
            if (!onGround)
            {
                return;
            }
            //get movement axis
            float move = Input.GetAxis("Horizontal");

            //gives the speed the value of our movement
            //speed cannot be negative in this case as fliping is 
            //done using Flip(), so we use absolute value for it
            //this is used for changing between IDLE-MOVE status if they exist
            anim.SetFloat("Speed", Mathf.Abs(move));




        }



        //input dependent actions should be put here for better precision
        void Update()
        {

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.LoadLevel("LevelSelectScene");
            }

            //freeze screen when starting the game and wait for tap to start
            if (canMove)
            {
                Time.timeScale = 1.0f;
            }
            if (!canMove)
            {
                Time.timeScale = 0f;
            }

            //reload level on death
            if (isDead)
            {
                maxSpeed = 0f;
                isStuck = true;
				rb.constraints = RigidbodyConstraints2D.FreezePositionY;
				animator.speed = 0;
				DeathTimer -= Time.deltaTime;
				if(DeathTimer < 0)
				{
					Application.LoadLevel(Application.loadedLevelName);
				}
               
            }


            curPos = transform.position.x;
            if (curPos == lastPos)
            {
                isStuck = true;
            }
            else if (curPos > lastPos)
            {
                isStuck = false;
            }
            lastPos = curPos;


            if (onGround || DoubleJump)
            {

                timeBetweenJumps = 0.0f;
            }
            else if (!DoubleJump)
            {
                timeBetweenJumps += Time.deltaTime;
            }




            //move character on X axis, Y remains the same
            GetComponent<Rigidbody2D>().velocity = new Vector2(maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

            //if we have pressed Space or Tap on mobile, we are not grounded anymore
            //and we need to jump with a certain Force
            if ((onGround || !DoubleJump) && (((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)) || Input.GetKeyDown(KeyCode.Space)))
            {

                //canMove = true;
                //make simple jump
                if (onGround && !DoubleJump && Time.timeScale > 0f)
                {

                    anim.SetBool("Ground", false);
                    //we can execute the jump in different modes

                    GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 0);
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, JumpForce));
					if (SoundEnabled) 
						GetComponent<AudioSource>().PlayOneShot(jumpSound, 0.5f);
                }
                //has made single jump, make double jump only if the second tap was fast enough, meaning less than 5s away from the first one
                else if (!onGround && !DoubleJump && timeBetweenJumps < 5f && Time.timeScale > 0f)
                {

                    anim.SetBool("Ground", false);
                    //we can execute the jump in different modes
                    GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, 0);
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, JumpForce));
                    DoubleJump = true;
					if(SoundEnabled)
						GetComponent<AudioSource>().PlayOneShot(jumpSound, 0.35f);
                }

            }



        }





        void OnTriggerEnter2D(Collider2D coll)

        {
            if (coll.tag == "killerObject")
            {
				GetComponent<AudioSource>().PlayOneShot(SplashSound, 1.5f);
                isDead = true;
            }

			//else if (coll.tag == "Screenshot")
			//{
			//	if(ApplicationModel.LivePlay)
			//		connectAPI.TakeScreenshot ();
			//}

            else if (coll.tag == "Coin")
            {


                HelperScript.score += 100;
                Destroy(coll.gameObject);
                //int rnd = Random.Range(1,8);
                GameObject coinParticles = (GameObject)Instantiate(Resources.Load("CST-PSG-1"));
                coinParticles.transform.position = new Vector2(coll.gameObject.transform.position.x, coll.gameObject.transform.position.y);

                coinParticles.GetComponent<ParticleSystem>().Play();
                //coinParticles.particleEmitter.emit = true;


                Destroy(coinParticles, 0.8f);
                GetComponent<AudioSource>().PlayOneShot(pickupCoin, 0.35f);
            }
            //if character hit the invisible colider with the tag closeToFinishMark, StartAppInterstitialShow.cs will load a StartApp interstitial Ad
            else if (coll.tag == "closeToFinishMark")
            {
                closeToFinish = true;
            }
            else if (coll.tag == "stopCameraMark")
            {
                stopCameraMark = true;
            }
            //the character has completed a level
            else if (coll.tag == "levelFinished")
            {
                //Application.LoadLevel("MenuScene");
				
                FinishLevel();
                /*
                //if the player has played this level before and has earned stars for it, get the number from PlayerPrefs
                int currentStarNumber = PlayerPrefs.GetInt(HelperScript.levelName + "StarCount", 0);

                //score is between -1 and 70% of maximum score - give one star
                if (((HelperScript.score > -1) && (HelperScript.score < ((0.7) * maxObtainableScore))) && currentStarNumber < 1)
                {

                    //give 1 star
                    PlayerPrefs.SetInt(HelperScript.levelName + "StarCount", 1);
                    UnlockNextLevel();
                }
                //score is between 70% and 90% of maximum score - give two stars
                else if (((HelperScript.score > ((0.7) * maxObtainableScore)) && (HelperScript.score < ((0.9) * maxObtainableScore))) && currentStarNumber < 2)
                {

                    PlayerPrefs.SetInt(HelperScript.levelName + "StarCount", 2);
                    UnlockNextLevel();
                }
                //score is between 90% and 100% of maximum score - give three stars
                else if ((HelperScript.score > ((0.9) * maxObtainableScore)) && currentStarNumber < 3)
                {
                    PlayerPrefs.SetInt(HelperScript.levelName + "StarCount", 3);
                    UnlockNextLevel();
                }
                
                LoadNextLevel();
                */
            }


        }

        void FinishLevel()
        {

            canMove = false;
			if (ApplicationModel.LivePlay) {
				Debug.Log ("finishlevel live play");
				connectAPI.sumarCompletado (() => {
					Debug.Log ("complete un nivel"); 
				});
				connectAPI.generarDescuento (() => {
					Debug.Log ("genere descuento del nivel" + Level); 
					Discount.text = PlayerPrefs.GetString ("Discount");
					LevelFinishedBackgroundLivePlay.SetActive (true);
				}, Level);
			} else {
				Debug.Log ("finishlevel practice mode");
				LevelFinishedBackgroundPracticeMode.SetActive (true);
			}


        }

        public void GoToNextLevel()
        {
            UnlockNextLevel();
            LoadNextLevel();
        }

        private void LoadNextLevel()
        {

            //if this is the final level
            if (PlayerPrefs.GetString("LastLevelName", null) == HelperScript.levelName)
            {
				Application.LoadLevel("MenuScene");

            }
            //if this is not the final level
            else
            {
                //increment the current level number, to load the next one
                HelperScript.currentLevelNumber++;
                //load the next level
                Application.LoadLevel("Level" + (HelperScript.currentLevelNumber).ToString());
            }

        }



        private void UnlockNextLevel()
        {   
            //if this is not the final level
            if (!(PlayerPrefs.GetString("LastLevelName", null) == HelperScript.levelName))
            {
                //unlock the next level
                PlayerPrefs.SetString("Level" + (HelperScript.currentLevelNumber + 1).ToString(), "unlocked");
                // Debug.Log("desbloquear nivel!!!!  Level"+ PlayerPrefs.GetInt("Level" + (HelperScript.currentLevelNumber + 1).ToString(), 0));
                
            }
             

        }



    }
}
