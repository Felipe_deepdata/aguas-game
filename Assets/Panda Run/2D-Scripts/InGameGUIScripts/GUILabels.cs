﻿using UnityEngine;
using System.Collections;

public class GUILabels : MonoBehaviour {

	public bool scoreLabel;
	public bool levelLabel;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(scoreLabel)
		{
			this.GetComponent<TextMesh>().text = "Score: " + HelperScript.score;	
		}
		if(levelLabel)
		{		
			this.GetComponent<TextMesh>().text = "Level "+ HelperScript.currentLevelNumber;
		}
	}
}
