﻿using UnityEngine;
using System.Collections;

public class btnWatchVideo : MonoBehaviour {

	public Sprite btnActive; //sprite representing active state of button
	private SpriteRenderer spriteRenderer;
	private Sprite initialSprite; 


	// Use this for initialization
	void Start () {
		initialSprite = this.GetComponent<SpriteRenderer>().sprite; //save object's sprite to an initial variable
		spriteRenderer = GetComponent<Renderer>() as SpriteRenderer;
	}
	

	void OnMouseDown()
	{
		spriteRenderer.sprite = btnActive;
	}

	//show video ad
	void OnMouseUp()
	{
		spriteRenderer.sprite = initialSprite;
	}

}
