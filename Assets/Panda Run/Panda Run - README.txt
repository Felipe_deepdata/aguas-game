

Panda Run - by IngeniouS Games Studio
*********************

=============================================================================================================================================================


SCRIPTS: 


		*** GAMEPLAY SCRIPTS ***

CharacterController.cs 
 - handles the character controller logic - switches run / jump animations, using the Animator Unity component 
								- handles jump/double jump
								- handles score
								- pickup items
								- plays sounds / particle effects
								- sets levels as finished / sets how many stars the player has earned
								
CameraStabilizer.cs 
	- moves the camera that follows the character

HelperScript.cs 
	- script used to store static variables, needed to pass data from one script to another, including from scripts used in different scenes.

MoveLevel.cs
	- script used to move the background as the player advances through level

TapToStartScript.cs
	- script used to unpause the game in the begining (the game starts with Time.timeScale = 0;)

GUILabels.cs 
	- script used to display the score and the level name / number

	The scripts insite the MenuScripts folder, are used in the MainMenu.unity scene, to handle button touches/clicks. They can be edited to 
play advertisment video or show picture ads on click, and also to open specific URL's (like more games or rating pages).



		*** LEVEL SELECT MANAGER ***

LevelSelectBack.cs
	- handles the back button (or Esc key) functionality in LevelSelectScene.unity scene, leading to MainMenu scene

NextLevelPage.cs
	- switches to the next page of levels (if you have more than one page)
	
SelectLevelClick.cs 
	- sets up the levels for the first time (all the necessary keys/values are stored in PlayerPrefs),
 when the project is ran on a new device (whether it is a computer with unity editor, or a mobile device runing a build) 
	
	- displays the level icons according to the data stored in Player Prefs for each level 
 
	The levels from the LevelSelectScene.unity are set up correctly, but in order to add new levels, or create a configuration from scratch, here is what you need 
to know: 
	- the level name must be in the following format: Level + number, and they must be set up in the correct order. For example: The first level must be named Level1, the second: Level2, the third Level3, etc.
Also the variable Level Number, assigned to each level must correspond to the number that is the level name's suffix: the fist level number will be 1, the second 2,
the third 3, and so on). 
	- the Level Name variable must not be null
	- the Locked By Default bool variable is used to determine whether the level will be set up as locked or unlocked when the initial setup occurs. Generally all
the levels should be locked by default, except the first level
	- the Is Last Level variable determines which level is the last in the configuration, so if you have 27 levels for example, level 27 will have
the Is Last Level variable set to true 
	
TIP: to quickly add new levels, you can:
	- duplicate an existing level page (select the level page and press Ctrl + D)
	- change the level names, numbers, level arguments, and Is Last Level variable on each of the newly obtained page levels( dont forget to uncheck IsLastLevel from
 the previous page if you set it to true on the last level of this new page)
	- add the new page gameobject to the array in the NextLevelPage.cs script, on the previousPage and nextPage buttons
	- You are all set!!!
	
=============================================================================================================================================================
*** You will also find comments explaining the implemented logic and variable roles inside the scripts ***


The maximum score that can be earned in each level can be calculated by the following formula:
	
	maxScore = coinsNumber * 100;

coinsNumber - the total number of coins found in the level
100 - the number of points earned for each picked up coin
maxScore - the maximum score for that level











