﻿using UnityEngine;
using System.Collections;

public class NextLevelPage : MonoBehaviour {
	[Tooltip("Array containing the gameObjects that represent the level pages. Must not be empty!!!")]
	public GameObject[] levelPages;
	public GameObject nextButton;
	public GameObject previousButton;
	// Use this for initialization
	void Start () {
	HelperScript.currentPage = 0;  //we always start by showing the first page of levels in the level select

		if(levelPages.Length > 0)
		{
			for (int i=0; i< levelPages.Length; i++)
			{
				if(i != HelperScript.currentPage)
				{
					levelPages[i].SetActive(false);
				}
				else
				{
					levelPages[i].SetActive(true);
				}
			}
		}
		else
		{
			Debug.Log ("levelPages[] array, of NextLevelPage.cs Script  cannot be empty");
		}

	}
	
	// Update is called once per frame
	void Update () {
		if(HelperScript.currentPage == 0)
		{
			previousButton.SetActive(false);
		}
		else
		{
			previousButton.SetActive(true);
		}
		
		if(HelperScript.currentPage == levelPages.Length - 1)
		{
			nextButton.SetActive(false);
		}
		else
		{
			nextButton.SetActive(true);
		}
	}



	void OnMouseDown()
	{	
		
		
	}
	
	
	/// <summary>
	/// Raises the mouse up event.
	/// </summary>
	void OnMouseUp()
	{

		if(levelPages.Length > 0)
		{

			if(transform.name == "nextPage")
			{
				HelperScript.currentPage ++;
				for (int i=0; i< levelPages.Length; i++)
				{
					if(i != HelperScript.currentPage)
					{
						levelPages[i].SetActive(false);
					}
					else
					{
						levelPages[i].SetActive(true);
					}
				}

			}

			if(transform.name == "previousPage")
			{
				Debug.Log ("previousPage");
				HelperScript.currentPage --;
				for (int i=0; i< levelPages.Length; i++)
				{
					if(i != HelperScript.currentPage)
					{
						levelPages[i].SetActive(false);
					}
					else
					{
						levelPages[i].SetActive(true);
					}
				}
			}
		}
		else
		{
			Debug.Log ("levelPages[] cannot be empty");
		}
	}
}
