﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Water : MonoBehaviour {

	public float Speed;
	Vector3 vibrationFactor;
	public float waveDelay;
	public float waveAmplification;

	public bool Rotate;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		vibrationFactor.y = Mathf.Sin (Time.time * Speed + waveDelay) * waveAmplification;
			transform.position += vibrationFactor;

		if (Rotate) {
			//transform.RotateAround (Vector3.forward, vibrationFactor.y);
			transform.Rotate (0,0, vibrationFactor.y*100);
		}
	}
}
