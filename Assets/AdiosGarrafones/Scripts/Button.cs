﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace AdiosGarrafones
{

    public class Button : MonoBehaviour
    {

		float vibratingTime = 0.1f;
		bool vibrating = false;
		public AudioClip ClickSound;

		public GameObject connectAPIGO;
		public ConnectAPI connectAPI;
		public Text SoundLabel;
		public Text MusicLabel;
		public MusicTrack musicTrack;

		public GameObject PreferencesPanel;

        public GameObject BtnSession;
        public GameObject Content;

		public Sprite Character1Thumbnail, Character2Thumbnail;
		private int lastlevel;


        // Use this for initialization
        void Start()
        {
			
			lastlevel = PlayerPrefs.GetInt ("_Arguments");
			Debug.Log ("Ultimo nivel completado" + PlayerPrefs.GetInt("level"));

            string user = PlayerPrefs.GetString("user");
			if (PlayerPrefs.GetInt("SoundEnabled")==1){
					AudioListener.pause = true;
			}
            if (SoundLabel != null) {
				if (PlayerPrefs.GetInt("SoundEnabled")==0) {
					Debug.Log("primera vez 1");
					SoundLabel.text = "ON";
					AudioListener.pause = false;
				}
				if (PlayerPrefs.GetInt("SoundEnabled")==1){
					Debug.Log("primera vez 2");
					SoundLabel.text = "OFF";
					AudioListener.pause = true;
				}
			}

			if (MusicLabel != null) {
				if (PlayerPrefs.GetInt("MusicEnabled")==0) {
					MusicLabel.text = "ON";
				}
				if (PlayerPrefs.GetInt("MusicEnabled")==1){
					MusicLabel.text = "OFF";
				}
			}
			
			if (Character1Thumbnail != null && Character2Thumbnail != null) {
				Image image = gameObject.GetComponent<Image> ();
				int Gender = PlayerPrefs.GetInt ("Gender", -1);
				Debug.Log("primera vez "+ Gender);
				if(Gender == 1){
					Debug.Log("igual a 1"+ Gender);
					image.sprite = Character1Thumbnail;
				}else{
					image.sprite = Character2Thumbnail;
				}
			}

			if (user == null || user.Equals(""))
            {
                BtnSession.SetActive(false);
            }
		
        }

		private void PlayClickSound(){
			GetComponent<AudioSource>().PlayOneShot(ClickSound, 1);
		}

        public void StartLiveGame()
        {
			PlayClickSound ();
			//Handheld.Vibrate();
            string user= PlayerPrefs.GetString("user");
			// Debug.Log("usuariooooo"+user);
            if (user == null || user.Equals(""))
                connectAPI.StartLiveGame();
            else
                connectAPI.LoginDirect();


        }

        public void closeSession()
        {
            PlayerPrefs.SetString("user", null);
			PlayerPrefs.SetString("DiscountCode", null);
            BtnSession.SetActive(false);
			Content.SetActive(false);
			Debug.Log("cerré sesion");
        }

        public void StartPracticeMode()
        {

			PlayClickSound ();
			ApplicationModel.LivePlay = false;
            Application.LoadLevel("LevelSelectScene");
			//Handheld.Vibrate();
        }

		public void ToggleMainCharacter(){
			Debug.Log("Toggle");
			int Gender = PlayerPrefs.GetInt("Gender");
			Debug.Log("antes de gender"+ Gender);
			if(Gender == null || Gender == 0 ){
				Gender += 1;
			}
			
			Gender *= -1;
			PlayerPrefs.SetInt ("Gender", Gender);
			if (Character1Thumbnail != null && Character2Thumbnail != null) {
				Image image = gameObject.GetComponent<Image> ();
				if(Gender == 1){
					image.sprite = Character1Thumbnail;
				}
				else{
					image.sprite = Character2Thumbnail;
				}
			}
	

			PlayClickSound ();
		}

		public void OpenPreferences(){
			if (PreferencesPanel != null) {
				PreferencesPanel.gameObject.SetActive (true);
				PlayClickSound ();
			}
		
		}

		public void ClosePreferences(){
			if (PreferencesPanel != null) {
				PreferencesPanel.gameObject.SetActive (false);
				PlayClickSound ();
			}
		}

        public void OpenAguagenteSite()
        {

            Application.OpenURL("http://www.aguagente.com/");
        }

		public void ShareDiscountCoupon(){
			//PlayClickSound ();
			connectAPIGO = GameObject.Find("ConnectAPI");
			connectAPI = connectAPIGO.GetComponent<ConnectAPI>();
			connectAPI.ShareCoupon();
		}

		IEnumerator Vibrate () {
			float time = 0;
			//Turn towards the side.
			while ( time <vibratingTime ) {
				time += Time.deltaTime;
				Handheld.Vibrate();
				yield return null;
			}

		}

		public void ToggleSound(){
			int SoundEnabled = PlayerPrefs.GetInt("SoundEnabled");
			if(SoundEnabled == 0)
				PlayerPrefs.SetInt("SoundEnabled", 1);
			if(SoundEnabled == 1)
				PlayerPrefs.SetInt("SoundEnabled", 0);

			SoundEnabled = PlayerPrefs.GetInt ("SoundEnabled");

			if (SoundLabel != null) {
				if (SoundEnabled == 1) {
					SoundLabel.text = "OFF";
					AudioListener.pause = true;
				}
				if (SoundEnabled == 0)  {
					SoundLabel.text = "ON";
					AudioListener.pause = false;
					PlayClickSound ();
				}
			}
		}

		public void ToggleMusic(){
			int MusicEnabled = PlayerPrefs.GetInt("MusicEnabled");
			if(MusicEnabled == 0)
				PlayerPrefs.SetInt("MusicEnabled", 1);
			if(MusicEnabled == 1)
				PlayerPrefs.SetInt("MusicEnabled", 0);

			MusicEnabled = PlayerPrefs.GetInt ("MusicEnabled");

			if (MusicLabel != null) {
				if (MusicEnabled == 1) {
					MusicLabel.text = "OFF";
					if (musicTrack != null)
						musicTrack.Stop ();
				}
				if (MusicEnabled == 0)  {
					MusicLabel.text = "ON";
					if (musicTrack != null)
						musicTrack.Play ();
				}
			}
		}




    }
}
