﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoButton : MonoBehaviour {


	public VideoSpieler videoPlayer;


	public AudioClip ClickSound;
	// Use this for initialization
	void Start () {
		
	}

	private void PlayClickSound(){
		GetComponent<AudioSource>().PlayOneShot(ClickSound, 1);
	}


	public void ShowVideo(){
		PlayClickSound ();
		videoPlayer.gameObject.SetActive (true);
		AudioListener.pause = true;
		videoPlayer.Play ();
		

	}

	public void HideVideo(){
		PlayClickSound ();
		videoPlayer.gameObject.SetActive (false);
		AudioListener.pause = false;
	}

	// Update is called once per frame
	void Update () {
		/*if (Input.touchCount > 0)
		{
			videoPlayer.gameObject.SetActive (false);
		}*/
	}
}
