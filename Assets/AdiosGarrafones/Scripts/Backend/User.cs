﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class User : System.Object
{
	public int id;
	public string idFacebook;
	public string FacebookName;
	public string email;
	public int Score;
	public string extra_info;
	public int intented;
	public int completed;
	public int maxscore;
	public string datemaxscore;
}
