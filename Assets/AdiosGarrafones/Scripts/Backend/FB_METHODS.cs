﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Facebook.Unity;
using Facebook;

public class FB_METHODS : System.Object
{


	private Action globalExecute;
	public  string Status = "Ready";
	private string LastResponse = string.Empty;

	public string _LastResponse
	{
		get { return this.LastResponse; }
		set { this.LastResponse = value; }
	}
	public void CallFBInit(Action execute)
	{
		this.globalExecute = execute;
		FB.Init(OnInitComplete, OnHideUnity);
	}

	public void OnInitComplete()
	{
		Debug.Log("FB.Init completed: Is user logged in? " + FB.IsLoggedIn);
		this.globalExecute ();
	}

	public void OnHideUnity(bool isGameShown)
	{
		Debug.Log("Is game showing? " + isGameShown);
	}


	public void CallFBLogin(Action execute)
	{
		//FB.Login("email, publish_actions, publish_stream, user_photos", LoginCallback);
			this.globalExecute = execute;
			FB.LogInWithReadPermissions (new List<string> () { "public_profile", "email", "user_friends" }, this.HandleResult);
		

	}

	public void FBUserid(Action execute){
		this.globalExecute = execute;
		FB.API("me?fields=name,email", Facebook.Unity.HttpMethod.GET,this.HandleResult);
	}

	public void FBInvitenousers(){
		string data="Invitación a jugar ";
		string title="Adios Garrafones";
		FB.AppRequest(data+title, callback: this.HandleResult);
	}

	public void HandleResult(IResult result)
	{
		if (result == null)
		{
			this.LastResponse = "Null Response\n";

			return;
		}
			
		// Some platforms return the empty string instead of null.
		if (!string.IsNullOrEmpty(result.Error))
		{
			this.Status = "Error - Check log for details";
			this.LastResponse = "Error Response:\n" + result.Error;
			//LogView.AddLog(result.Error);
		}
		else if (result.Cancelled)
		{
			this.Status = "Cancelled - Check log for details";
			this.LastResponse = "Cancelled Response:\n" + result.RawResult;
			//LogView.AddLog(result.RawResult);
		}
		else if (!string.IsNullOrEmpty(result.RawResult))
		{
			this.Status = "Success - Check log for details";
			this.LastResponse = result.RawResult;
			Debug.Log(this.LastResponse);
			this.globalExecute ();
			//LogView.AddLog(result.RawResult);
		}
		else
		{
			this.LastResponse = "Empty Response\n";
			//LogView.AddLog(this.LastResponse);
		}
	}

}