﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using System.Text;

public class RESTApi : System.Object {

	public string url_api;
	public string parameters;
	public string result;

	public IEnumerator GET(Action execute){
		UnityWebRequest www = UnityWebRequest.Get(this.url_api+this.parameters);
		yield return www.Send();

		if(www.isError) {
			Debug.Log(www.error);
			this.result="E";
		}
		else {
			this.result=www.downloadHandler.text;
		}
		execute();

	}

	private UnityWebRequest SendPostRequest(string url,string parameters,string method){
		UnityWebRequest request=new UnityWebRequest(this.url_api,method);
		byte[] bytes = Encoding.UTF8.GetBytes(parameters);
		request.SetRequestHeader("Content-Type", "application/json");
		UploadHandlerRaw uh = new UploadHandlerRaw (bytes);
		request.uploadHandler = uh;
		request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer ();
	
		return request;


	}
	public IEnumerator POST(Action execute){
		UnityWebRequest www = SendPostRequest(this.url_api,this.parameters,UnityWebRequest.kHttpVerbPOST);

        yield return www.Send();
			if(www.isError) {
				Debug.Log(www.error);
				this.result="E";
			}
			else {
				this.result = www.downloadHandler.text;
			}
		execute();
	}

	public IEnumerator PUT(Action execute){
		UnityWebRequest www = SendPostRequest(this.url_api,this.parameters,UnityWebRequest.kHttpVerbPUT);
		yield return www.Send();
		if(www.isError) {
			Debug.Log(www.error);
			this.result="E";
		}
		else {
			this.result = www.downloadHandler.text;
		}
		execute();
	}
		
	public Boolean isResultError(){
		return this.result.Equals ("E");
	}




}
