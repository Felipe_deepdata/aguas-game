﻿using UnityEngine;
using System.Collections;

public class Truck : MonoBehaviour {

	public float Speed;
	bool VisibleByCamera = false;
	public Vector3 ExhaustOffset = new Vector3(2f,0f,0f);
    Camera mainCamera;
    private bool activated = false;

    Vector3 vibrationFactor;
	// Use this for initialization
	void Start () {
        mainCamera = Camera.main;
        StartCoroutine("CheckDistance");
    }

	void OnBecameVisible() {
		VisibleByCamera = true;
		//StartCoroutine ("EmitExhaust");
	}

    IEnumerator CheckDistance()
    {
        if (activated)
            yield break;
        while (!activated)
            {
                if (Vector3.SqrMagnitude(transform.position - mainCamera.transform.position) < 180)
                {
                    StartCoroutine("EmitExhaust");
                    activated = true;
                }
                yield return new WaitForSeconds(0.1f);
            }



    }

    void OnBecameInvisible(){
		VisibleByCamera = false;
	}

	IEnumerator EmitExhaust(){
		for (;;) {
				Instantiate (Resources.Load ("TruckExhaust"), transform.position + ExhaustOffset, Quaternion.identity);
				yield return new WaitForSeconds (0.8f);
		}
	
	}

	// Update is called once per frame
	void Update () {

        vibrationFactor.y = Mathf.Sin(Time.time*100)*0.025f;

        if (activated)
        {
            transform.position += Vector3.left * Time.deltaTime * Speed + vibrationFactor;
        }
	}
}
