﻿using UnityEngine;
using System.Collections;

public class WaterJetController : MonoBehaviour {

	public Animator anim;
	bool visible = true;
	// Use this for initialization
	void Start () {
	
	}

	void OnBecameVisible() {
		visible = true;
		anim.SetBool ("VisibleByCamera", true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
