﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscountCode : MonoBehaviour {

	public GameObject Content;

	public Text DiscountLabel;
	public InputField DiscountCodeField;
	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetString ("DiscountCode") != null && PlayerPrefs.GetString ("DiscountCode")!= "" ) {
			ShowDiscountCode ();
		
		}


	}



	public void ShowDiscountCode(){

		if (Content != null)
		Content.SetActive (true);

		if (DiscountLabel != null)
			DiscountLabel.text = "Tienes un codigo para: "+PlayerPrefs.GetString ("Discount");

		if(DiscountCodeField != null)
			DiscountCodeField.text = PlayerPrefs.GetString("DiscountCode");

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
